#pragma once
#include <SFML/Graphics.hpp> // Library needed for using sprites, textures and fonts
#include <vector> // Library for handling collections of objects
#include "Bullet.h"
#include <SFML/Audio.hpp>

class Player
{
	public: // Access level (to be discussed later)

	// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer);

	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	void Reset();
	void DrawTo(sf::RenderTarget& target);

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	// Setters
	void SetAlive(bool newAlive);

private:

	std::vector<Bullet>& bullets;

	// Variables used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	sf::Vector2u screenBounds;
	float speed;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	bool alive;
};

