#include "Player.h"

Player::Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer)
	: sprite(playerTexture)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, screenBounds(newScreenBounds)
	, bullets(newBullets)
	, bulletTexture(newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax (sf::seconds(0.5f))
	, bulletFireSound(firingSoundBuffer)
{
	Reset();
}

void Player::Input()
{
	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;

		// Play firing Sound
		bulletFireSound.play();

		bullets.push_back(Bullet(bulletTexture, screenBounds, bulletPosition, sf::Vector2f(1000, 0)));
		
		bulletCooldownRemaining = bulletCooldownMax;
	}
}

void Player::Update(sf::Time frameTime)
{
	// Move the player
	sprite.setPosition(sprite.getPosition() + velocity * frameTime.asSeconds());

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;
}

void Player::Reset()
{
	sprite.setPosition(
		screenBounds.x / 2 - sprite.getTexture()->getSize().x / 2 - 900,
		screenBounds.y / 2 - sprite.getTexture()->getSize().y / 2);

	velocity.x = 0.0f;
	velocity.y = 0.0f;
}

void Player::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}

bool Player::GetAlive()
{
	return alive;
}

sf::FloatRect Player::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Player::SetAlive(bool newAlive)
{
	alive = newAlive;
}
