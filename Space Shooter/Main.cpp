// Library Includes
#include <SFML/Graphics.hpp> // Library for creating and managing SFML windows.
#include <SFML/Audio.hpp> // Library for using audio in SFML
#include <string> // Library for manipulating strings of text
#include <vector> // To allow us the use of vector collections
#include "Player.h" // Include our own class definitions
#include "Star.h"
#include "Bullet.h"
#include <cstdlib> // Random numbers
#include <time.h>
#include "Enemy.h"
#include "SpawnData.h"

int main()
{
	// Declare the SFML window, called gameWindow 
	sf::RenderWindow gameWindow;

	// Set up the SFML window and pass in the dimmensions and name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Space Shooter", sf::Style::Titlebar | sf::Style::Close);
	
	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------

	// Bullets
	std::vector<Bullet> playerBullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet.png");

	std::vector<Bullet> enemyBullets;
	sf::Texture enemyBulletTexture;
	enemyBulletTexture.loadFromFile("Assets/Graphics/enemyBullet.png");

	//Player Sprite
	// Declare an image, called playerTexture
	sf::Texture playerTexture;

	// Assign the player.png from the file to the playerTexture
	playerTexture.loadFromFile("Assets/Graphics/player.png");

	// Fire Sound
	sf::SoundBuffer firingSoundBuffer;
	firingSoundBuffer.loadFromFile("Assets/Audio/fire.ogg");
	sf::Sound bulletFireSound;
	bulletFireSound.setBuffer(firingSoundBuffer);

	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize(), playerBullets, playerBulletTexture, firingSoundBuffer);

	// Movement Patterns
	std::vector< sf::Vector2f > zigZagPattern;
	zigZagPattern.push_back(sf::Vector2f(900, 900));
	zigZagPattern.push_back(sf::Vector2f(600, 300));
	zigZagPattern.push_back(sf::Vector2f(300, 900));

	std::vector< sf::Vector2f > zagZigPattern;
	zagZigPattern.push_back(sf::Vector2f(700, 200));
	zagZigPattern.push_back(sf::Vector2f(500, 700));
	zagZigPattern.push_back(sf::Vector2f(300, 200));

	// Enemies
	std::vector<Enemy> enemies;

	// Declare an image, called enemyTexture
	sf::Texture enemyTexture;

	// Assign the enemy.png from the file to the enemyTexture
	enemyTexture.loadFromFile("Assets/Graphics/enemy.png");

	enemies.push_back(Enemy(enemyTexture, gameWindow.getSize(), enemyBullets, enemyBulletTexture, firingSoundBuffer, sf::Vector2f(gameWindow.getSize().x - 100, gameWindow.getSize().y / 2), zigZagPattern));
	
	// Spawn info
	std::vector<SpawnData> spawnDataVector;
	int spawnIndex = 0;
	
	spawnDataVector.push_back({ sf::Vector2f(gameWindow.getSize().x - 100,100), zigZagPattern, sf::seconds(1.0f) });
	spawnDataVector.push_back({ sf::Vector2f(gameWindow.getSize().x - 100,500), zigZagPattern, sf::seconds(2.0f) });
	spawnDataVector.push_back({ sf::Vector2f(gameWindow.getSize().x - 100,800), zagZigPattern, sf::seconds(1.0f) });
	spawnDataVector.push_back({ sf::Vector2f(gameWindow.getSize().x - 100,200), zagZigPattern, sf::seconds(2.0f) });

	sf::Time timeToSpawn = spawnDataVector[0].delay;

	// Game Music
	// Declare a music variable, called gameMusic
	sf::Music gameMusic;

	// Open the music.ogg file and play it
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();

	// Game Font
	// Declare a Font variable, called gameFont
	sf::Font gameFont;

	// Load the mainFont.ttf from the file
	gameFont.loadFromFile("Assets/Font/mainFont.ttf");

	// Title Text
	// Declare a Text variable, called titletext
	sf::Text titleText;

	// Set titleText's font to gameFont
	titleText.setFont(gameFont);

	// Set the string that will display for titleText
	titleText.setString("Space Shooter");

	// Set the different parameters of titleText
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::White);
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	titleText.setPosition(
		gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	// Score
	// Declare an integer variable, called score and initalise it to 0
	int score = 0;

	// Declare a Text variable, called scoreText
	sf::Text scoreText;

	// Set scoreText's font to gameFont
	scoreText.setFont(gameFont);

	// Set the string that will display for scoreText as well as the integer value of score
	scoreText.setString("Score: " + std::to_string(score));

	// Set the different parameters of scoreText
	scoreText.setCharacterSize(24);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	// Stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star> stars;
	int numStars = 5;
	for (int i = 0; i < numStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}

	// Game Clock
	// Declare a Clock variable, called gameClock
	sf::Clock gameClock;

	sf::Vector2f playerVelocity(0.0f, 0.0f);

	float speed = 100.0f;

	// Game Over
	// Game over variable to track if the game is done
	bool gameOver = false;

	// Loss and Win sounds
	sf::SoundBuffer lossBuffer;
	lossBuffer.loadFromFile("Assets/Audio/loss.ogg");
	sf::Sound lossSFX(lossBuffer);

	sf::SoundBuffer winBuffer;
	winBuffer.loadFromFile("Assets/Audio/win.ogg");
	sf::Sound winSFX(winBuffer);

	// Text 
	sf::Text gameOverText;
	gameOverText.setFont(gameFont);

	// Set the string that will display for gameOverText
	gameOverText.setString("GAME OVER\n\nPress R to restart\nPress Q to quit");

	// Set the different parameters of gameOverText
	gameOverText.setCharacterSize(34);
	gameOverText.setFillColor(sf::Color::White);
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	gameOverText.setPosition(
		gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, gameWindow.getSize().y / 2 - gameOverText.getLocalBounds().height / 2);

	sf::Text winText;
	winText.setFont(gameFont);

	// Set the string that will display for winText
	winText.setString("YOU WIN\n\nPress R to restart\nPress Q to quit");

	// Set the different parameters of winText
	winText.setCharacterSize(34);
	winText.setFillColor(sf::Color::White);
	winText.setStyle(sf::Text::Bold | sf::Text::Italic);
	winText.setPosition(
		gameWindow.getSize().x / 2 - winText.getLocalBounds().width / 2, gameWindow.getSize().y / 2 - winText.getLocalBounds().height / 2);

	// Win state
	bool winState = false;

	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;

		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This will section repeat for each event waiting to be processed

			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		}// End of polling loop

		// Player keybind input
		playerObject.Input();

		if (gameOver || winState)
		{
			// Check if we should quit the game
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				// Quit the game
				return 0;
			}

			// Check if we should restart the game
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				// Restart the game
				gameOver = false;
				winState = false;
				score = 0;
				gameMusic.play();
				enemies.clear();
				enemyBullets.clear();
				playerBullets.clear();
				spawnIndex = 0;
			}
		}

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------
		
		// Get the time passed since the last frame and restart the game clock
		sf::Time frameTime = gameClock.restart();

		if (!gameOver && !winState)
		{
			// Move the player
			playerObject.Update(frameTime);

			// Move the enemy
			for (int i = enemies.size() - 1; i >= 0; --i)
			{
				enemies[i].Update(frameTime);

				if (!enemies[i].GetAlive())
				{
					enemies.erase(enemies.begin() + i);
				}
			}

			// Update the scoreText string to match the score variable
			scoreText.setString("Score: " + std::to_string(score));

			// Update the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].Update(frameTime);
			}

			// Update the player bullets
			for (int i = playerBullets.size() - 1; i >= 0; --i)
			{
				playerBullets[i].Update(frameTime);

				if (!playerBullets[i].GetAlive())
				{
					playerBullets.erase(playerBullets.begin() + i);
				}
			}

			// Update the enemy bullets
			for (int i = enemyBullets.size() - 1; i >= 0; --i)
			{
				enemyBullets[i].Update(frameTime);

				if (!enemyBullets[i].GetAlive())
				{
					enemyBullets.erase(enemyBullets.begin() + i);
				}
			}

			// Check if it is time to spawn an enemy
			timeToSpawn -= frameTime;

			if (timeToSpawn <= sf::seconds(0) && spawnIndex < spawnDataVector.size())
			{
				// Spawn the enemy at the position with the pattern
				enemies.push_back(Enemy(enemyTexture, gameWindow.getSize(), enemyBullets, enemyBulletTexture, firingSoundBuffer, spawnDataVector[spawnIndex].position, spawnDataVector[spawnIndex].pattern));

				spawnIndex++;

				if (spawnIndex < spawnDataVector.size())
				{
					timeToSpawn = spawnDataVector[spawnIndex].delay;
				}
			}

			// Check for collisons between the player's bulets and enemies
			for (int i = 0; i < playerBullets.size(); i++)
			{
				for (int j = 0; j < enemies.size(); j++)
				{
					// Get the two bounding boxes
					sf::FloatRect bulletBounds = playerBullets[i].GetHitBox();
					sf::FloatRect enemyBounds = enemies[j].GetHitBox();

					// Check if they overlap
					if (bulletBounds.intersects(enemyBounds))
					{
						// Kill the bullet and the enemy
						playerBullets[i].SetAlive(false);
						enemies[j].SetAlive(false);

						// Add to the score
						score += 100;
					}

				}

				// Check if we won
				if (spawnIndex >= spawnDataVector.size() && enemies.empty())
				{
					winState = true;
					gameMusic.stop();
					winSFX.play();
				}
			}

			// Check for collisions between the enemy bullets and the player
			for (int i = 0; i < enemyBullets.size(); i++)
			{
				// Get the two bounding boxes
				sf::FloatRect bulletBounds = enemyBullets[i].GetHitBox();
				sf::FloatRect playerBounds = playerObject.GetHitBox();

				// Check if they overlap
				if (bulletBounds.intersects(playerBounds))
				{
					// Kill the bullet and the player
					enemyBullets[i].SetAlive(false);
					playerObject.SetAlive(false);
					gameOver = true;
					gameMusic.stop();
					lossSFX.play();
				}

			}
		}
		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------

		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Black);

		// Draw everything to the window
		gameWindow.draw(titleText);

		// Only draw these items if the game has not ended
		if (!gameOver && !winState)
		{
			// Draw the player
			playerObject.DrawTo(gameWindow);

			// Draw the score text
			gameWindow.draw(scoreText);
			
			// Draw the enemy
			for (int i = 0; i < enemies.size(); ++i)
			{
				enemies[i].DrawTo(gameWindow);
			}

			// Draw the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].DrawTo(gameWindow);
			}

			// Draw the player bullets
			for (int i = 0; i < playerBullets.size(); ++i)
			{
				playerBullets[i].DrawTo(gameWindow);
			}

			// Draw the enemy bullets
			for (int i = 0; i < enemyBullets.size(); ++i)
			{
				enemyBullets[i].DrawTo(gameWindow);
			}
		}

		if (gameOver)
		{
			// Draw game over text to the window
			gameWindow.draw(gameOverText);
		}

		if (winState)
		{
			// Draw game over text to the window
			gameWindow.draw(winText);
		}

		// Display the window contents on the screen 
		gameWindow.display();
	}

	return 0;
}