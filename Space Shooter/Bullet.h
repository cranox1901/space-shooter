#pragma once
// Library Includes
#include <SFML/Graphics.hpp>
#include <vector>

class Bullet
{
public:

	// Constructor
	Bullet(sf::Texture& bulletTexture, sf::Vector2u newScreenBounds, sf::Vector2f startingPosition, sf::Vector2f newVelocity);

	// Public Functions
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	// Setters
	void SetAlive(bool newAlive);

private:

	// Variables
	sf::Sprite sprite;
	sf::Vector2f velocity;
	sf::Vector2u screenBounds;
	bool alive;
};

