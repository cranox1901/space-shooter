#pragma once
#include <SFML/Graphics.hpp> // Library needed for using sprites, textures and fonts
#include <vector> // Library for handling collections of objects
#include "Bullet.h"
#include <SFML/Audio.hpp>

class Enemy
{
public: // Access level (to be discussed later)

// Constructor
	Enemy(sf::Texture& enemyTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer, sf::Vector2f startingPosition, std::vector< sf::Vector2f > newMovementPattern);

	// Functions to call specific code
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);

	// Getters
	bool GetAlive();
	sf::FloatRect GetHitBox();

	// Setters
	void SetAlive(bool newAlive);

private:

	std::vector<Bullet>* bullets;

	// Variables used by this class
	sf::Sprite sprite;
	sf::Vector2f velocity;
	sf::Vector2u screenBounds;
	float speed;
	sf::Texture* bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	std::vector< sf::Vector2f > movementPattern;
	int currentInstruction;
	bool alive;
};
