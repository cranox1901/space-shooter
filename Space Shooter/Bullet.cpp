#include "Bullet.h"

Bullet::Bullet(sf::Texture& bulletTexture, sf::Vector2u newScreenBounds, sf::Vector2f startingPosition, sf::Vector2f newVelocity)
{
	sprite.setTexture(bulletTexture);

	screenBounds = newScreenBounds;

	sprite.setPosition(startingPosition);

	velocity = newVelocity;

	alive = true;
}

void Bullet::Update(sf::Time frameTime)
{
	// Calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	// Have we gone off the screen completely to the left or right?
	if (newPosition.x + sprite.getTexture()->getSize().x < 0 || newPosition.x > screenBounds.x)
	{
		alive = false;
	}

	// Move to the new position
	sprite.setPosition(newPosition);
}

bool Bullet::GetAlive()
{
	return alive;
}

sf::FloatRect Bullet::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Bullet::SetAlive(bool newAlive)
{
	alive = newAlive;
}

void Bullet::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}
