#include "Enemy.h"

Enemy::Enemy(sf::Texture& enemyTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture& newBulletTexture, sf::SoundBuffer& firingSoundBuffer, sf::Vector2f startingPosition, std::vector< sf::Vector2f > newMovementPattern)
	: sprite(enemyTexture)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, screenBounds(newScreenBounds)
	, bullets(&newBullets)
	, bulletTexture(&newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax(sf::seconds(0.5f))
	, bulletFireSound(firingSoundBuffer)
	, movementPattern(newMovementPattern)
	, currentInstruction(0)
	, alive(true)

{
	sprite.setPosition(startingPosition);
}

void Enemy::Update(sf::Time frameTime)
{
	// If we reach the end of our pattern, do nothing
	if (currentInstruction >= movementPattern.size())
	{
		// Kill him...kill him now
		alive = false;

		// abort
		return;
	}

	// Get target from instruction
	sf::Vector2f targetPoint = movementPattern[currentInstruction];

	// Get distance vector from the targetPoint
	sf::Vector2f distanceVector = targetPoint - sprite.getPosition();

	// Calculate direction vector, by dividing distanceVector by its magnitude
	float distanceMag = std::sqrt(distanceVector.x * distanceVector.x + distanceVector.y * distanceVector.y);
	sf::Vector2f directionVector = distanceVector / distanceMag;

	float distanceToTravel = speed * frameTime.asSeconds();

	// Calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + directionVector * distanceToTravel;

	// Check if we will reach or overshoot our target
	if (distanceMag <= distanceToTravel)
	{
		//We have reached the target
		newPosition = targetPoint;

		// Start moving to the next target
		++currentInstruction;
	}

	// Move the enemy
	sprite.setPosition(newPosition);

	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;

	// if the ccoldown is up shoot abullet
	if (bulletCooldownRemaining <= sf::seconds(0.0f))
	{
		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture->getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture->getSize().x / 2;

		// Play firing Sound
		bulletFireSound.play();

		bullets->push_back(Bullet(*bulletTexture, screenBounds, bulletPosition, sf::Vector2f(-1000, 0)));

		//reset cooldown
		bulletCooldownRemaining = bulletCooldownMax;


	}
}


void Enemy::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}

bool Enemy::GetAlive()
{
	return alive;
}

sf::FloatRect Enemy::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Enemy::SetAlive(bool newAlive)
{
	alive = newAlive;
}
